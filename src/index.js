import LoginUserService from './services/user/login' 

const login = new LoginUserService();

export default {
  services: {
    user: {
      login: login
    }
  }
};
